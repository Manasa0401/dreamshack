package com.imaginnovate.learninghub.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.imaginnovate.learninghub.entity.Enroll;

public interface EnrollRepository extends JpaRepository<Enroll, Long> {

}
