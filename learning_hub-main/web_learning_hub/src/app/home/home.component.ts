import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  img: any = '../../assets/learning1.jpg';
  bannerTitle: any = 'The Best Learning Experience you can achieve';
  bannerSubTitle: any = 'The most important principle for learning is the more you share the more you gain, keep upgrading yourself';
}
