import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EventsService } from '../services/Events/events-service.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
})
export class EventsComponent {
  header = 'Upcoming Events';
  cardDetails: any = [];
  img:any =  '../../assets/learning4.jpg';
  bannerTitle: any = 'Live video is becoming a vital tool for IT Employees';
  bannerSubTitle: any = 'Live events are always impactful when they consist benefits with real-world examples.Watch them now';
  specificCardDetails: any = [];
  constructor(public router: Router, private eventsService: EventsService) {
    this.header = this.router.url === '/home' ? 'Upcoming Events' : 'Events';
  }

  ngOnInit() {
    if (this.router.url === '/home') {
      this.eventsService.getUpCommingEvents().subscribe((data: any) => {
        this.specificCardDetails = data.splice(0, 4);
      });
    } else {
      const selected = { eventType: 'jbhunt' };
      this.eventsService.getEventTypeList(selected.eventType).subscribe((data: any) => {
        this.specificCardDetails = data;
        this.specificCardDetails = this.specificCardDetails.filter(
          (data: any) => data.eventType === selected.eventType
        );
      });
    }
  }
}
