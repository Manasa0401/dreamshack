import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import {
  ConfirmationService,
  ConfirmEventType,
  MessageService,
} from 'primeng/api';
import { EventsService } from 'src/app/services/Events/events-service.service';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss'],
  providers: [ConfirmationService, MessageService],
})
export class EventCardComponent {
  @Input() heading: any;
  @Input() specificCardDetails: any;
  display: boolean = false;
  time: any = '2023-02-14T14:00:00';
  dateFormat = 'YYYY-DD-MM';
  timeFormat = 'hh:mm A';
  constructor(
    public router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private eventsService: EventsService
  ) {
    // time and date format
    // const testDateUtc = moment.utc(this.time);
    // const localDate = testDateUtc.local();
    // console.log(localDate.format(this.dateFormat));
    // console.log(localDate.format(this.timeFormat));
  }

  goToEventPage() {
    this.router.navigate(['/events']);
  }

  selectedType(event: any) {
    this.eventsService.getEventsList().subscribe((data: any) => {
      this.specificCardDetails = data;
      this.specificCardDetails = this.specificCardDetails.filter(
        (data: any) => data.eventType === event.eventType
      );
    });
  }

  showDialog() {
    this.display = true;
  }

  enroll(id: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Confirmed',
          detail: 'You are successfully enrolled',
        });
        this.specificCardDetails.map((data: any) => {
          if (data.id === id) {
            data.count = data.count + 1;
          }
        });
      },
      reject: (type: any) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({
              severity: 'error',
              summary: 'Rejected',
              detail: 'You have rejected',
            });
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({
              severity: 'warn',
              summary: 'Cancelled',
              detail: 'You have cancelled',
            });
            break;
        }
      },
    });
  }
}
