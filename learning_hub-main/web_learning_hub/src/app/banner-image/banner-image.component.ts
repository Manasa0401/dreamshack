import { Component, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-banner-image',
  templateUrl: './banner-image.component.html',
  styleUrls: ['./banner-image.component.scss']
})
export class BannerImageComponent {
  @Input() image:any;
  @Input() bannerTitle: any;
  @Input() bannerSubTitle: any;
  constructor(private elem: ElementRef){
    this.elem.nativeElement.style.setProperty('--background-image', this.image);
  }

}
