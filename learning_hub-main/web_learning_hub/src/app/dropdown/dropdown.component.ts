import { Component } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { EventsService } from '../services/Events/events-service.service';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})

export class DropdownComponent {
  @Output() selectedType = new EventEmitter<string>();
  dropdownList: any = [
      {"id": 1, "name": "Jbhunt", "eventType": "jbhunt"
      },
      {"id": 2, "name": "Soft Skills", "eventType": "communication"
      },
      {"id": 3, "name": "Technology", "eventType": "technology"
      },
      {"id": 4, "name": "Logistics", "eventType": "logistics"
      }
  ];
    constructor(private eventsService: EventsService,) {
    }

    OnchangeValue(event: any) {
      this.selectedType.emit(event.value);
    }
}
