import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { EventsComponent } from './events/events.component';
import { CoursesComponent } from './courses/courses.component';
import { EventCardComponent } from './events/event-card/event-card.component';
import { CardModule } from 'primeng/card';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { HttpClientModule } from '@angular/common/http';
import { TooltipModule } from 'primeng/tooltip';
import { BannerImageComponent } from './banner-image/banner-image.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { FooterComponent } from './footer/footer.component';
import { CreateEventComponent } from './events/create-event/create-event.component';
import {DialogModule} from 'primeng/dialog';
import { CoursesCardComponent } from './courses/courses-card/courses-card.component';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {CalendarModule} from 'primeng/calendar';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    EventsComponent,
    CoursesComponent,
    BannerImageComponent,
    EventCardComponent,
    DropdownComponent,
    FooterComponent,
    CreateEventComponent,
    CoursesCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CardModule,
    PanelModule,
    DropdownModule,
    FormsModule,
    ButtonModule,
    DividerModule,
    ConfirmDialogModule,
    ToastModule,
    HttpClientModule,
    TooltipModule,
    DialogModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    CalendarModule
  ],
  providers: [ConfirmationService, MessageService],
  bootstrap: [AppComponent],
})
export class AppModule {}
