import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CoursesService } from '../services/Courses/courses-service.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent {
  heading = 'Explore Courses';
  coursesDetails: any = [];
  img:any =  '../../assets/learning5.jpg';
  bannerTitle: any = 'Great things are initially trained';
  bannerSubTitle: any = 'Training empowers people to freshen up their brains';
  constructor(public router: Router, private coursesService: CoursesService) {
    this.heading = this.router.url === '/home' ? 'Explore Courses' : 'Courses';
  }

  ngOnInit() {
    if (this.router.url === '/home') {
      this.coursesService.getCoursesList().subscribe((data: any) => {
        this.coursesDetails = data.splice(0,7);
      });
    } else {
      const selected = { eventType: 'jbhunt' };
      this.coursesService.getCoursesList().subscribe((data: any) => {
        this.coursesDetails = data;
        this.coursesDetails = this.coursesDetails.filter(
          (data: any) => data.courseType === selected.eventType
        );
      });
    }
  }
}
