import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CoursesService } from 'src/app/services/Courses/courses-service.service';

@Component({
  selector: 'app-courses-card',
  templateUrl: './courses-card.component.html',
  styleUrls: ['./courses-card.component.scss']
})
export class CoursesCardComponent {
 @Input() coursesDetails: any;
 @Input() heading: any;
 constructor(public router: Router, private coursesService: CoursesService) {
 }

 selectedType(event: any) {
  this.coursesService.getCoursesList().subscribe((data: any) => {
    this.coursesDetails = data;
    this.coursesDetails = this.coursesDetails.filter(
      (data: any) => data.courseType === event.eventType
    );
  });
}

 goToCourses() {
  this.router.navigate(['/courses']);
}

goToLink(url: string){
  window.open(url, "_blank");
}
}
