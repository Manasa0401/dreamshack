import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private http: HttpClient) { }
  BaseUrl = 'http://localhost:8082';

  getEventsList() {
    return this.http.get<any>(`${this.BaseUrl}/learninghub/getallevents`);
  }

  getEventTypeList(eventType: any) {
    return this.http.get<any>(`${this.BaseUrl}/learninghub/getByEventType/${eventType}`);
  }

  getUpCommingEvents() {
    return this.http.get<any>(`${this.BaseUrl}/learninghub/latestEvents`);
  }
}
