import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) { }
  BaseUrl = 'http://localhost:8082';

getCoursesList() {
  return this.http.get<any>(`${this.BaseUrl}/learninghub/getallcourses`);
}
}
